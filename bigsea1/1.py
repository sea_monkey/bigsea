# 命名规则（变量，常量，函数，属性，方法，类，模块，包）：
# 1. 区分大小写
# 2. 首字符是_或者字母，不能是数字；首字母以外可以是_，或者字母，或者数字。
# 3. 关键字不能使用
# 4. 内置函数不能使用

# 关键字(首字母大写3个，其余均小写)
# 1. False
# 2. None
# 3. True
# 4. def
# 5. if
# 6. raise
# 7. del
# 8. import
# 9. return
# 10. elif
# 11. in
# 12. try
# 13. and
# 14. else
# 15. is
# 16. while
# 17. as
# 18. except
# 19. lambda
# 20. with
# 21. assert
# 22. finally
# 23. nonlocal
# 24. yield
# 25. break
# 26. for
# 27. not
# 28. class
# 29. from
# 30. or
# 31. continue
# 32. global
# 33. pass

# 快捷键
# Ctrl+/ : 注释，取消注释
